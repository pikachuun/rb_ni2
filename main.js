'use strict';

//required for multi-OS/niche feature support
global.fs = require("fs");
global.util = require('util');
global.EXEC = require("child_process").spawn;
global.LOADED = []; //push all extra files that get loaded into here, if you don't mind. don't add full directories, just add relative ones (i.e. js/cmd.js)
global.INTERVALS = []; //similar to the above, push all intervals into here (particularly used in pixiv-related things)
global.PROMPTS = {}; //all guild-specific prompts will go into here
global.path = require("path");

//discord/pixiv APIs
try {
	require("discord.js");
	require("pixiv-api-client");
	require("better-sqlite3");
} catch (e) { //Most likely not installed?
	console.log("Dependencies not found or out of date. Running npm install...")
	require("child_process").execSync(`npm install`, {stdio:"inherit", cwd: __dirname});
}
global.DISCORD = require("discord.js");
global.PIXIVAPI = require("pixiv-api-client");
global.SQL = require("better-sqlite3");
global.CLIENT = new DISCORD.Client({
	partials: ["MESSAGE","CHANNEL","REACTION"],
	presence: {status:"online"},
	intents: 32767, //All of them, just to be safe
}); 
global.MessageEmbed = DISCORD.MessageEmbed;
global.PIXIV = new PIXIVAPI();
global.deepStringify = function (obj) {
	let seenObj = [obj];
	let stringStep = function (obj) {
		if (obj === undefined) { return "undefined"; }
		if (obj === null) { return "null"; }
		switch (typeof obj) {
			case "object":
				if (obj.constructor === Array) {
					let str = "[";
					for (let i of obj) {
						if (str !== "[") { str += ","; }
						if (typeof i === "object" && i !== null && seenObj.indexOf(i) !== -1) {
							str += `"{"_ISCIRCULAR":true}`;
						} else {
							if (typeof i === "object" && i !== null) { seenObj.push(i); }
							str += `${stringStep(i)}`;
						}
					}
					return str+"]";
				} else if (obj.constructor === Object) {
					let str = "{";
					for (let k in obj) {
						if (str !== "{") { str += ","; }
						if (typeof obj[k] === "object" && obj[k] !== null && seenObj.indexOf(obj[k]) !== -1) {
							str += `"${k.replace(/"/g, `\\"`)}":{"_ISCIRCULAR":true}`;
						} else {
							if (typeof obj[k] === "object" && obj[k] !== null) { seenObj.push(obj[k]); }
							str += `"${k.replace(/"/g, `\\"`)}":${stringStep(obj[k])}`;
						}
					}
					return str+"}";
				} else {
					return obj.toString() || "{}"; //If I can't replicate it then I'll just return an empty object to prevent crashes
				}
			case "function":
				return obj.toString();
			case "string":
				return `"${obj.replace(/"/g, `\\"`)}"`;
			default: 
				return String(obj);
		}
	}
	return stringStep(obj);
}
global.deStringify = function (str) {
	if (!str) { return undefined; } //duh
	try {
		let destr = str;
		eval(`destr = ${destr}`); //ponponPAIN (there is no other way to store and restore functions)
		return destr;
	} catch (e) {
		console.log(`Error on destringing:\n${e}`);
		return undefined;
	}
}

//Load config
let reqVer = "2.0.0";
try {
	LOADED.push("config.js");
	require(path.resolve("config.js"));
	if (reqVer !== configVer) { throw "ver"; }
	if (!token) { throw "token"; }
	if (!pixivRefreshToken) {
		console.log("A token related to the pixiv API wasn't specified.\nWhile this won't prevent the bot from running, the bot's pixiv listener functionality will be unusable.\nIf you need these functions, please ensure you've specified the token properly, and try again. pixiv_auth.py may be useful here.");
	}
	if (verbose) { console.log("config OK"); }
} catch (e) {
	switch (e) {
		case "ver":
			console.log("An incorrect version of the config was detected. Please make a fresh copy of config_base.js, rename to config.js, and update the settings appropriately.");
			break;
		case "token":
			console.log("A token for the bot wasn't specified. Please ensure you've made a bot application in the Discord Developer Portal and provided its token, then try again.");
			break;
		default:
			console.log("Something went wrong when loading the config, or it doesn't exist. Please make a fresh copy of config_base.js, rename to config.js, and update the settings appropriately.");
	}
	process.exit(1);
}

//Main Database
console.log("sorting out the main database...");
if (!fs.existsSync(path.resolve("data"))) { fs.mkdirSync(path.resolve("data")); }
global.DB = new SQL("data/rb_ni2.db", { verbose: (verbose ? (s) => { console.log(`SQLExec: ${s}`); } : null) });
DB.pragma("journal_mode = WAL;"); //Use write-ahead
DB.pragma("synchronymous = normal;"); //Don't wait on fsync
DB.pragma("temp_store = memory;"); //Precaution
DB.pragma("mmap_size = 4294967296;"); //Precaution^2 (change if desired)
DB.pragma("auto_vacuum = incremental;"); //Run once at the start
setInterval(() => {
	if (verbose) { console.log(`Doing a bit of database maintenance...`); }
	DB.pragma("wal_checkpoint(TRUNCATE);"); //handle all (just in case)
	DB.pragma("optimize;"); //Run once every few hours
	DB.pragma("incremental_vacuum;"); //Regular Cleanup
}, 10000000); //a bit less than 3 hours

//Initialize essential tables if they don't already exist
{
	DB.prepare(`CREATE TABLE IF NOT EXISTS "guilds" ("id" TEXT UNIQUE ON CONFLICT IGNORE, "mainInfo" TEXT, "pixivInfo" TEXT);`).run();
	let columns = DB.pragma(`table_info("guilds")`);
	//columns is formatted as an array of objects
		//cid: column ID
		//name: duh
		//type: duh (does not include UNIQUE/etc.)
		//notnull: 0 or 1
		//dflt_value: it's the default value
		//pk: 0 or 1 (primary key)
	//SQL doesn't allow for duplicate columns so I can theoretically just do this
	//let singleColumn = DB.prepare(`ALTER TABLE "guilds" ADD COLUMN $columnName STRING`);
}
//DB Functions so I don't have to type them every time
//call with func.run(args)
global.DBFUNC = {
	setGuildKey: {
		//it's impossible to specify an argument as a column (probably because it checks to see if it exists?)
		//so we uh... do this instead
		mainInfo: DB.prepare(`INSERT INTO "guilds" ("id", "mainInfo") VALUES (@guildID, @guildInfo) ON CONFLICT ("id") DO UPDATE SET "mainInfo"=@guildInfo`),
		pixivInfo: DB.prepare(`INSERT INTO "guilds" ("id", "pixivInfo") VALUES (@guildID, @guildInfo) ON CONFLICT ("id") DO UPDATE SET "pixivInfo"=@guildInfo`),
	},
	delGuildKey: {
		//it's impossible to specify an argument as a column (probably because it checks to see if it exists?)
		//so we uh... do this instead
		mainInfo: DB.prepare(`UPDATE "guilds" SET "mainInfo"=NULL`),
		pixivInfo: DB.prepare(`UPDATE "guilds" SET "pixivInfo"=NULL`),
	},
	fetchGuildKey: {
		//While we CAN specify an argument as a column here, it always uses '' when parsed, rather than ""
		//This means we can never actually return anything
		//so... bleh
		mainInfo: DB.prepare(`SELECT DISTINCT "mainInfo" FROM "guilds" WHERE "id" = @guildID`),
		pixivInfo: DB.prepare(`SELECT DISTINCT "pixivInfo" FROM "guilds" WHERE "id" = @guildID`),
	},
}
global.setGuildKey = function (guildID, key, info) {
	try {
		if (!DBFUNC.setGuildKey[key]) { throw "Column does not exist in table."; }
		let guildInfo = deepStringify(info); //Required to properly parse the function
		DBFUNC.setGuildKey[key].run({guildID, guildInfo});
		return true;
	} catch (e) {
		console.log(`Error in setGuildKey(${guildID}, ${key}, ${info}):\n${e}`);
		return false;
	}
}
global.delGuildKey = function (guildID, key) {
	try {
		if (!DBFUNC.delGuildKey[key]) { throw "Column does not exist in table."; }
		DBFUNC.delGuildKey[key].run({guildID});
		return true;
	} catch (e) {
		console.log(`Error in delGuildKey(${guildID}, ${key}, ${info}):\n${e}`);
		return false;
	}
}
global.fetchGuildKey = function (guildID, key) {
	try {
		if (!DBFUNC.fetchGuildKey[key]) { throw "Column does not exist in table."; }
		let keyInfo = DBFUNC.fetchGuildKey[key].pluck().get({guildID});
		return deStringify(keyInfo);
	} catch (e) {
		console.log(`Error in fetchGuildKey(${guildID}, ${key}):\n${e}`);
		return undefined;
	}
}

//We can close this on exit
process.on('exit', () => {
	DB.pragma("optimize;"); //Run once upon closure
	DB.close();
});
process.on('SIGHUP', () => process.exit(128 + 1));
process.on('SIGINT', () => process.exit(128 + 2));
process.on('SIGTERM', () => process.exit(128 + 15));

//Actually log in and start executing features
console.log("yep, it's login time");
CLIENT.login(token);
delete global.token; //Security

//Condensing this into a function
global.BOOT = function (reboot=false) {
	if (reboot) {
		for (let i of INTERVALS) {
			clearInterval(i.interval);
			if (i.mcList && i.mcList.length) { //there are message collectors in addition to standard intervals
				for (let mc of i.mcList) {
					mc.stop();
				}
			}
		}
		INTERVALS = [];
		for (let f of LOADED) {
			delete require.cache[path.resolve(f)];
		}
		LOADED = ["config.js"];
		require(path.resolve("config.js"));
		if (verbose) { console.log("config OK"); }
		//Purge listeners; we'll reinitialize our essentials during the bootstrap
		CLIENT.removeAllListeners();
	}
	//Load the main files
	//If there are any others to be loaded, please do so in one of these two for easy reloading later
	//Commands:
	try {
		require(path.resolve("js/cmd.js"));
		if (verbose) { console.log("commandlist OK"); }
	} catch (e) {
		console.log(`Error initializing commandlist. Terminating as this is a critical functionality for the bot to function properly...\n${e}`);
		process.exit(1);
	}
	LOADED.push("js/cmd.js");
	//Client Main Loop:
	try {
		require(path.resolve("js/clientloop.js"));
		if (verbose) { console.log("clientloop OK"); }
	} catch (e) {
		console.log(`Error initializing clientloop. Terminating as this is a critical functionality for the bot to function properly...\n${e}`);
		process.exit(1);
	}
	LOADED.push("js/clientloop.js");
}

//Add an entry if a guild is joined
CLIENT.on('guildCreate', async (guild) => {
	guildInfo = fetchGuildKey(guild.id, "mainInfo");
	if (!guildInfo) {
		guildInfo = {};
		setGuildKey(guild.id, "mainInfo", {}); //Bind to database
	}
});

//Ready
CLIENT.on('ready', async () => {
	console.log(`Client is logged in, finalizing everything else...`);
	BOOT();
	if (discordAvatar) {
		if (verbose) { console.log(`Avatar being set to ${discordAvatar}...`); }
		try {
			await CLIENT.user.setAvatar(discordAvatar);
		} catch (e) {
			if (verbose) { console.log("Didn't work, but eh, I'm probably being rate-limited. It's whatever."); }
		}
		delete global.discordAvatar; //For the sake of privacy in case of filepath
	} 
	if (discordStatus) {
		if (typeof discordStatus === "function") { //Execute the function
			if (verbose) { console.log(`Processing status...`); }
			let out = discordStatus();
			if (typeof out === "string") { CLIENT.user.setActivity(out); }
		} else {
			if (verbose) { console.log(`Status being set to ${discordStatus}...`); }
			CLIENT.user.setActivity(discordStatus);
			setInterval(() => { //transient interval that will DB through reloads
				CLIENT.user.setActivity(discordStatus); //Yes, again
			}, 21600000); //6 hours
		}
	}
	console.log(`we booted successfully... but will we...... crash.......????????`);
	setTimeout(() => {
		console.log(`boggers, dude! we didn't crash minute 1! LET'S GO I STILL GOT IT`);
	}, 60000);
});